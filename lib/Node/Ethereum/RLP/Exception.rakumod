module Node::Ethereum::RLP::Exception {
    class X is Exception {
        has Str $.payload is default('invalid RLP');

        method message returns Str {
            return sprintf("%s: %s", self.^name, $!payload);
        }
    }

    class X::Decode is X {};
    class X::Decode::Length is X::Decode {};
}
