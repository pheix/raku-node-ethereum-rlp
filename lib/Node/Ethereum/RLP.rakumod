unit class Node::Ethereum::RLP;

use Node::Ethereum::RLP::Exception;

multi method rlp_encode(buf8 :$input!) returns buf8 {
    my $inputbuf = buf8.new($input.list);

    if $inputbuf.bytes == 1 and $inputbuf[0] < 128 {
        return $inputbuf;
    }
    else {
        return $inputbuf.prepend(self!encode_length(:len($inputbuf.bytes), :offset(128)));
    }
}

multi method rlp_encode(Str :$input!) returns buf8 {
    my $inputbuf = buf8.new($input.encode);

    if $inputbuf.bytes == 1 and $inputbuf[0] < 128 {
        return $inputbuf;
    }
    else {
        return $inputbuf.prepend(self!encode_length(:len($inputbuf.bytes), :offset(128)));
    }
}

multi method rlp_encode(:@input!) returns buf8 {
    my $buf = buf8.new;

    for @input.values -> $v {
        $buf.push(self.rlp_encode(:input($v)));
    }

    return $buf.prepend(self!encode_length(:len($buf.bytes), :offset(192)));
}

multi method rlp_decode(buf8 :$input!, Bool :$decode = True) returns Hash {
    my $length;
    my $llength;
    my $data;
    my $innerRemainder;
    my $d;

    my @decoded;

    my $firstByte = $input.list.head;

    if $firstByte <= 0x7f {
        return {
            data      => $input.subbuf(0, 1),
            remainder => $input.subbuf(1, *),
        }
    } elsif $firstByte <= 0xb7 {
        $length = $firstByte - 0x7f;

        if $firstByte == 0x80 {
            $data = buf8.new;
        } else {
            $data = $input.subbuf(1, $length - 1);
        }

        if $length == 2 && $data.first < 0x80 {
            die X::Decode.new(:payload('byte must be less 0x80'));
        }

        return {
            data      => $data,
            remainder => $input.subbuf($length, *),
        }
    } elsif $firstByte <= 0xbf {
        $llength = $firstByte - 0xb6;

        if ($input.bytes - 1) < $llength {
            die X::Decode::Length.new(:payload('not enough bytes for string length'));
        }

        $length = self!safe_parse_int(:v($input.subbuf(1 .. ($llength - 1))), :base(16));

        if $length <= 55 {
            die X::Decode::Length.new(:payload('expected string length to be greater than 55'));
        }

        $data = $input.subbuf($llength, $length);

        if $data.bytes < $length {
            die X::Decode::Length.new(:payload('not enough bytes for string'));
        }

        return {
          data      => $data,
          remainder => $input.subbuf($length + $llength, *);
        }
    } elsif $firstByte <= 0xf7 {
        $length = $firstByte - 0xbf;

        $innerRemainder = $input.subbuf(1, $length - 1);

        while ($innerRemainder.bytes) {
            $d = self.rlp_decode(:input($innerRemainder), :decode($decode));

            $d<data> = $d<data>.decode if $d<data> !~~ Array && $decode;

            @decoded.push($d<data>);
            $innerRemainder = $d<remainder>;
        }

        return {
            data      => @decoded,
            remainder => $input.subbuf($length, *)
        }
    } else {
        $llength = $firstByte - 0xf6;
        $length  = self!safe_parse_int(:v($input.subbuf(1 .. ($llength - 1))), :base(16));

        my UInt $totalLength = $llength + $length;

        if $totalLength > $input.bytes {
            die X::Decode::Length.new(:payload('total length is larger than the data'));
        }

        $innerRemainder = $input.subbuf($llength, $totalLength - 1);

        if !$innerRemainder.bytes {
            die X::Decode::Length.new(:payload('list has a invalid length'));
        }

        while ($innerRemainder.bytes) {
            $d = self.rlp_decode(:input($innerRemainder), :decode($decode));

            $d<data> = $d<data>.decode if $d<data> !~~ Array && $decode;

            @decoded.push($d<data>);
            $innerRemainder = $d<remainder>;
        }

        return {
            data      => @decoded,
            remainder => $input.subbuf($totalLength, *)
        }
    }

    return {};
}

method int_to_hex(UInt :$x!) returns Str {
    my Str $hex = $x.base(16).Str;

    return $hex.chars % 2 ?? q{0} ~ $hex !! $hex;
}

method !encode_length(Int :$len!, Int :$offset!) returns buf8 {
    if $len < 56 {
        return buf8.new($len + $offset);
    }
    elsif $len < 256**8 {
        my Str  $hexlen    = self.int_to_hex(:x($len));
        my UInt $llength   = ($hexlen.chars / 2).UInt;
        my Str  $firstbyte = self.int_to_hex(:x($offset + 55 + $llength));

        buf8.new((sprintf("%s%s", $firstbyte, $hexlen) ~~ m:g/../).map({ :16($_.Str) }));
    }
    else {
        die X.new(:payload('input too long'));
    }
}

method !safe_parse_int(buf8 :$v, UInt :$base) returns Int {
    my Str $int;

    for $v.list -> $member {
        my $hex = $member.base(16);

        $int ~= ($member < 16 ?? '0' ~ $hex !! $hex);
    }

    if $int ~~ /^^ '00' / {
        die X.new(:payload('extra zeros while parsing integer'));
    }

    return $int.parse-base($base);
}
