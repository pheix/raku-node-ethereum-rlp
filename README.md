# Recursive Length Prefix for Raku

The purpose of RLP (Recursive Length Prefix) is to encode arbitrarily nested arrays of binary data, and RLP is the main encoding method used to serialize objects in Ethereum. `Node::Ethereum::RLP` delivers RLP encode and decode routines to Raku lang ecosystem.

## License

Module `Node::Ethereum::RLP` is free and opensource software, so you can redistribute it and/or modify it under the terms of the [The Artistic License 2.0](https://opensource.org/licenses/Artistic-2.0).

## Credits

1. https://eth.wiki/fundamentals/rlp
2. https://github.com/ethereumjs/rlp/blob/master/src/index.ts

## Author

Please contact me via [LinkedIn](https://www.linkedin.com/in/knarkhov/) or [Twitter](https://twitter.com/CondemnedCell). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).
