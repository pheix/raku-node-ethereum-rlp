use v6.d;
use Test;

use Node::Ethereum::RLP;

constant lorem = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit';
constant arr55 = ['dog', 'raku', ['dog', 'raku', 'dog'], 'god', ['dog', 'raku', 'dog'], ['Lorem', 'ipsum']];
constant arr56 = ['dog', 'raku', ['dog', 'raku', 'dog'], 'god', ['dog', 'raku', 'dog'], ['Lorem', 'ipsum'], ['dolor', 'sit', 'amet'], ['dog', 'raku', ['dolor', ['Lorem', 'ipsum'], 'sit', 'amet'], 'dog']];

plan 8;

use-ok 'Node::Ethereum::RLP';

my $rlp = Node::Ethereum::RLP.new;

is buf_to_hex($rlp.rlp_encode(:input(q{}))), '80', 'null is null';
is buf_to_hex($rlp.rlp_encode(:input('dog'))), '83646F67', 'dog is encoded';
is buf_to_hex($rlp.rlp_encode(:input('raku'))), '8472616B75', 'raku is encoded';

is buf_to_hex($rlp.rlp_encode(:input(['dog', 'raku', ['dog', 'raku']]))),
    'd383646f678472616b75c983646f678472616b75'.uc,
        'array is encoded';

is $rlp.rlp_decode(:input($rlp.rlp_encode(:input(lorem))))<data>.decode, lorem, 'lorem is decode';
is-deeply $rlp.rlp_decode(:input($rlp.rlp_encode(:input(arr55))))<data>, arr55, 'nested arrays less then 55 bytes';

#todo 'a list over 55 bytes long is not supported yet';
is-deeply $rlp.rlp_decode(:input($rlp.rlp_encode(:input(arr56))))<data>, arr56, 'nested arrays more then 55 bytes';

done-testing;

sub buf_to_hex(buf8 $buf) returns Str {
    my @encoded;

    for $buf.list -> $member {
        my $hex = $member.base(16);
        @encoded.push($member < 16 ?? '0' ~ $hex !! $hex);
    }

    return @encoded.join(q{});
}
